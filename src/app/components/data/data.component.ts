import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { noIgual, noAquino } from "../../validations/validations";

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styles: []
})
export class DataComponent implements OnInit {

  forma: FormGroup;

  usuario: Object = {
    nombrecompleto: {
      nombre: 'Maxi',
      apellido: 'Aquino'
    },
    email: 'maxiaquino0@gmail.com',
    //pasatiempoes: ['Correr', 'Dormir']
  };

  sexo = ['Femenino', 'Masculino'];

  autos = [
    {marca: 'Ford', modelos: ['Ka', 'Ecosport', 'Mondeo', 'Fiesta']},
    {marca: 'Chevrolet', modelos: ['Onix', 'Trucker', 'Cruze', 'Prisma']}
  ];
  modelos = [];

  constructor() {
    this.forma = new FormGroup({
      nombrecompleto: new FormGroup({
        nombre: new FormControl('', [Validators.required, Validators.minLength(5)]),
        apellido: new FormControl('', [Validators.required, Validators.minLength(5), noAquino])
      }),
      email: new FormControl('', [
                                  Validators.required,
                                  Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')
                                ]),
      pasatiempos: new FormArray([
        new FormControl('Correr', [Validators.required])
      ]),
      username: new FormControl('', Validators.required, this.existeUsuario),
      password1: new FormControl('', Validators.required),
      password2: new FormControl(''),
      marca: new FormControl('', Validators.required),
      modelo: new FormControl('', Validators.required),
      sexo: new FormControl('', Validators.required)
    });

    //this.forma.setValue(this.usuario);

    this.forma.controls['password2'].setValidators([
      Validators.required, noIgual.bind(this.forma)
    ]);

    this.forma.controls['marca'].valueChanges.subscribe(
      data => {
        this.modelos = this.autos.filter(auto => auto.marca === data)[0].modelos;
        console.log('modelos', this.modelos);
      }
    );
  }

  ngOnInit() {
  }

  agregarPasatiempo() {
    (<FormArray>this.forma.controls['pasatiempos']).push(
      new FormControl('', Validators.required)
    );
  }

  guardar() {
    console.log(this.forma);
    // this.forma.reset(this.usuario);
  }

  existeUsuario(control: FormControl): Promise<any>|Observable<any> {
    let promesa = new Promise(
      (resolve, reject) => {
        setTimeout(() => {
          if (control.value === 'strider') {
            resolve({existe: true});
          } else {
            resolve(null);
          }
        }, 3000);
      }
    );
    return promesa;
  }

}
