import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PaisService } from 'src/app/services/pais.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styles: [ ]
})
export class TemplateComponent implements OnInit {

  usuario: Object = {
    nombre: null,
    apellido: null,
    email: null,
    pais: '',
    sexo: null,
    acepta: false
  };

  paises: any[] = [];

  sexos = ['Femenino', 'Masculino'];

  constructor(private paisService: PaisService) { }

  ngOnInit() {
    this.paisService.getPaises().subscribe(
      resp => {
        console.log(resp);

        this.paises = resp;
      }
    );
  }

  guardar(f: NgForm) {
    console.log('Formulario posteado');
    console.log(f);
  }
}
