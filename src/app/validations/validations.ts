import { FormControl } from '@angular/forms';

export function noAquino(control: FormControl): {[s: string]: boolean} {
    if (control.value === 'Aquino') {
      return {
        noaquino: true
      };
    }

    return null;
  }

export function noIgual(control: FormControl): {[s: string]: boolean} {
    let forma: any = this;

    if (control.value !== forma.controls['password1'].value) {
      return {
        noigual: true
      };
    }

    return null;
  }